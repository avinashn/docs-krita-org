# Translation of docs_krita_org_reference_manual___brushes___brush_engines___pixel_brush_engine.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___brushes___brush_engines___pixel_brush_engine\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:27+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:1
msgid "The Pixel Brush Engine manual page."
msgstr "Сторінка підручника щодо рушія піксельних пензлів."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:16
msgid "Pixel Brush Engine"
msgstr "Рушій піксельних пензлів"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Рушій пензлів"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:19
msgid ".. image:: images/icons/pixelbrush.svg"
msgstr ".. image:: images/icons/pixelbrush.svg"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:20
msgid ""
"Brushes are ordered alphabetically. The brush that is selected by default "
"when you start with Krita is the :guilabel:`Pixel Brush`. The pixel brush is "
"the traditional mainstay of digital art. This brush paints impressions of "
"the brush tip along your stroke with a greater or smaller density."
msgstr ""
"Пензлі у списку упорядковуються за абеткою. Типовим пензлем, який буде "
"вибрано після запуску Krita, є :guilabel:`Піксельний пензель`. Піксельний "
"пензель є традиційною основою цифрового малювання. Цей пензель відтворює "
"роботу звичайного кінчика пензля, додаючи мазки на полотно вздовж руху "
"пензля із більшою або меншою щільністю."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:24
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:25
msgid "Let's first review these mechanics:"
msgstr "Спочатку наведемо огляд механіки:"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:27
msgid ""
"Select a brush tip. This can be a generated brush tip (round, square, star-"
"shaped), a predefined bitmap brush tip, a custom brush tip or a text."
msgstr ""
"Вибір кінчика пензля. Це може бути створений кінчик пензля (круглий, "
"квадратний, зіркоподібний), попередньо визначений растровий кінчик пензля, "
"нетиповий кінчик пензля або текст."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:28
msgid ""
"Select the spacing: this determines how many impressions of the tip will be "
"made along your stroke"
msgstr ""
"Вибір інтервалу. Визначає, скільки образів кінчика буде намальовано уздовж "
"вашого руху пензлем на полотні."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:29
msgid ""
"Select the effects: the pressure of your stylus, your speed of painting or "
"other inputs can change the size, the color, the opacity or other aspects of "
"the currently painted brush tip instance -- some applications call that a "
"\"dab\"."
msgstr ""
"Вибір ефектів: тиск на стило, швидкість малювання або інші вхідні дані "
"можуть змінювати розмір, колір, непрозорість та інші параметри поточного "
"намальованого екземпляра кінчика пензля — у деяких програмах такий екземпляр "
"називається «мазком»."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:30
msgid ""
"Depending on the brush mode, the previously painted brush tip instance is "
"mixed with the current one, causing a darker, more painterly stroke, or the "
"complete stroke is computed and put on your layer. You will see the stroke "
"grow while painting in both cases, of course!"
msgstr ""
"Залежно від режиму роботи пензля, раніше намальований екземпляр кінчика "
"пензля змішується із поточним, даючи темніший, зафарбованіший мазок або уся "
"послідовність мазків накладається на ваш шар. Ви, звичайно ж, у обох "
"випадках бачитимете як збільшується замальована область!"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:32
msgid ""
"Since 4.0, the Pixel Brush Engine has Multithreaded brush-tips, with the "
"default brush being the fastest mask."
msgstr ""
"Починаючи з версії 4.0, у рушії піксельних пензлів передбачено "
"багатопотокові кінчики пензлів. Типовий пензель має найшвидшу маску."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:34
msgid "Available Options:"
msgstr "Доступні параметри:"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:36
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:37
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:38
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:39
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:40
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:41
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:42
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:43
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:44
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:45
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:46
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:47
msgid ":ref:`option_source`"
msgstr ":ref:`option_source`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:48
msgid ":ref:`option_mix`"
msgstr ":ref:`option_mix`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:49
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:50
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:51
msgid ":ref:`option_masked_brush`"
msgstr ":ref:`option_masked_brush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:54
msgid "Specific Parameters to the Pixel Brush Engine"
msgstr "Специфічні для рушія піксельних пензлів параметри"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:57
msgid "Darken"
msgstr "Затемнений"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:59
msgid "Allows you to Darken the source color with Sensors."
msgstr ""
"Надає вам змогу визначити залежність затемнення початкового кольору від "
"даних датчиків."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:63
msgid ""
"The color will always become black in the end, and will work with Plain "
"Color, Gradient and Uniform random as source."
msgstr ""
"Найтемнішим кольором завжди є чорний. Працює з джерелами простого кольору, "
"градієнта і рівномірно розподіленого випадкового значення."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:66
msgid "Hue, Saturation, Value"
msgstr "Відтінок, Насиченість, Значення"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:68
msgid ""
"These parameters allow you to do an HSV adjustment filter on the :ref:"
"`option_source` and control it with Sensors."
msgstr ""
"За допомогою цих параметрів можна визначити фільтр коригування HSV для :ref:"
"`option_source` і керувати ним за допомогою даних датчиків планшета."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:71
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:72
msgid "Works with Plain Color, Gradient and Uniform random as source."
msgstr ""
"Працює з джерелами простого кольору, градієнта і рівномірно розподіленого "
"випадкового значення."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:75
msgid "Uses"
msgstr "Використання"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:78
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:79
msgid ""
"Having all three parameters on Fuzzy will help with rich color texture. In "
"combination with :ref:`option_mix`, you can have even finer control."
msgstr ""
"Встановлення для усіх трьох параметрів нечіткого значення допомагає "
"збагатити кольорову текстуру. У поєднанні із :ref:`option_mix` ви можете "
"налаштувати усе ще точніше."
