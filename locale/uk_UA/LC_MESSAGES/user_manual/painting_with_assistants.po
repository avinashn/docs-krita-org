# Translation of docs_krita_org_user_manual___painting_with_assistants.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_user_manual___painting_with_assistants\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 11:36+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<rst_epilog>:1
msgid ".. image:: images/assistants/Assistants_fish-eye_2_02.png"
msgstr ".. image:: images/assistants/Assistants_fish-eye_2_02.png"

#: ../../user_manual/painting_with_assistants.rst:2
msgid "How to use the painting assistants in Krita to draw perspectives."
msgstr ""
"Як користуватися допоміжними засобами малювання у Krita для малювання "
"перспективи."

#: ../../user_manual/painting_with_assistants.rst:12
msgid "Painting Assistants"
msgstr "Допоміжні засоби малювання"

#: ../../user_manual/painting_with_assistants.rst:17
msgid "Painting with Assistants"
msgstr "Малювання із допоміжними засобами"

#: ../../user_manual/painting_with_assistants.rst:19
msgid ""
"The assistant system allows you to have a little help while drawing straight "
"lines or circles."
msgstr ""
"Система допоміжних засобів малювання надає вам допомогу у малюванні прямих "
"ліній та кіл."

#: ../../user_manual/painting_with_assistants.rst:22
msgid ""
"They can function as a preview shape, or you can snap onto them with the "
"freehand brush tool. In the tool options of free hand brush, you can toggle :"
"guilabel:`Snap to Assistants` to turn on snapping."
msgstr ""
"Вони можуть працювати як засоби попереднього перегляду форми або ви можете "
"увімкнути у програмі прилипання до них для інструмента пензля довільного "
"малювання. У параметрах інструмента пензля довільного малювання ви можете "
"увімкнути :guilabel:`Прилипати до допоміжних ліній`, щоб увімкнути "
"прилипання."

#: ../../user_manual/painting_with_assistants.rst:30
msgid ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Krita's vanishing point assistants in action"
msgstr ""
".. image:: images/assistants/Krita_basic_assistants.png\n"
"   :alt: Робота допоміжних засобів нескінченно віддаленої точки Krita"

#: ../../user_manual/painting_with_assistants.rst:30
msgid "Krita's vanishing point assistants in action"
msgstr "Робота допоміжних засобів нескінченно віддаленої точки у Krita"

#: ../../user_manual/painting_with_assistants.rst:32
msgid "The following assistants are available in Krita:"
msgstr "У Krita можна скористатися такими допоміжними засобами:"

#: ../../user_manual/painting_with_assistants.rst:35
msgid "Types"
msgstr "Типи"

#: ../../user_manual/painting_with_assistants.rst:37
msgid ""
"There are several types in Krita. You can select a type of assistant via the "
"tool options docker."
msgstr ""
"У Krita передбачено декілька типів допоміжних засобів. Ви можете вибрати тип "
"допоміжного засобу за допомогою бічної панелі параметрів інструмента."

#: ../../user_manual/painting_with_assistants.rst:43
msgid "Ellipse"
msgstr "Еліпс"

#: ../../user_manual/painting_with_assistants.rst:45
msgid "An assistant for drawing ellipses and circles."
msgstr "Допоміжний засіб для малювання еліпсів і кіл."

#: ../../user_manual/painting_with_assistants.rst:47
msgid ""
"This assistant consists of three points: the first two are the axis of the "
"ellipse, and the last one is to determine its width."
msgstr ""
"Робота цього допоміжного інструмента визначається трьома точками: перші дві "
"є кінцями осі еліпса, а остання визначає його ширину."

#: ../../user_manual/painting_with_assistants.rst:51
msgid ""
"The same an ellipse, but allows for making ellipses that are concentric to "
"each other."
msgstr ""
"Те саме, що і еліпс, але уможливлює створення еліпсів, які є концентричними "
"один відносно одного."

#: ../../user_manual/painting_with_assistants.rst:52
msgid "Concentric Ellipse"
msgstr "Концентричний еліпс"

#: ../../user_manual/painting_with_assistants.rst:54
#: ../../user_manual/painting_with_assistants.rst:144
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines. Press the :kbd:`Shift` "
"key while holding the third handle, and it'll snap to a perfect circle."
msgstr ""
"Якщо ви утримуватимете натиснутою клавішу :kbd:`Shift` при визначенні перших "
"двох точок, ці точки буде розташовано на одній вертикалі або горизонталі. "
"Утримуйте натиснутою клавішу :kbd:`Shift` під час визначення третьої точки, "
"щоб отримати ідеальне коло."

#: ../../user_manual/painting_with_assistants.rst:61
#: ../../user_manual/painting_with_assistants.rst:154
msgid "Perspective"
msgstr "Перспектива"

#: ../../user_manual/painting_with_assistants.rst:63
msgid "This ruler takes four points and creates a perspective grid."
msgstr ""
"Для визначення цієї лінійки слід вказати чотири точки. Результат визначатиме "
"ґратку перспективи."

#: ../../user_manual/painting_with_assistants.rst:65
msgid ""
"This grid can be used with the 'perspective' sensor, which can influence "
"brushes."
msgstr ""
"Цією ґраткою можна скористатися у поєднанні із датчиком перспективи, який "
"впливає на поведінку пензлів."

#: ../../user_manual/painting_with_assistants.rst:68
msgid ""
"If you press the :kbd:`Shift` key while holding any of the corner handles, "
"they'll snap to one of the other corner handles, in sets."
msgstr ""
"Якщо ви утримуватимете натиснутою клавішу :kbd:`Shift` під час пересування "
"будь-якої з кутових точок, вони прилипатимуть до однієї з інших кутових "
"точок і утворюватимуть набори."

#: ../../user_manual/painting_with_assistants.rst:74
#: ../../user_manual/painting_with_assistants.rst:78
msgid "Ruler"
msgstr "Лінійка"

#: ../../user_manual/painting_with_assistants.rst:76
msgid "There are three assistants in this group:"
msgstr "У цій групі три допоміжних засоби:"

#: ../../user_manual/painting_with_assistants.rst:79
msgid "Helps create a straight line between two points."
msgstr "Допомагає у створенні прямої лінії між двома точками."

#: ../../user_manual/painting_with_assistants.rst:81
msgid "Infinite Ruler"
msgstr "Нескінченна лінійка"

#: ../../user_manual/painting_with_assistants.rst:81
msgid ""
"Extrapolates a straight line beyond the two visible points on the canvas."
msgstr "Екстраполює пряму лінію за двома видимим точками на полотні."

#: ../../user_manual/painting_with_assistants.rst:84
msgid ""
"This ruler allows you to draw a line parallel to the line between the two "
"points anywhere on the canvas."
msgstr ""
"За допомогою цієї лінійки ви зможете малювати лінію, яка буде паралельною до "
"ліній між двома точками у будь-якій позиції на полотні."

#: ../../user_manual/painting_with_assistants.rst:85
msgid "Parallel Ruler"
msgstr "Паралельна лінійка"

#: ../../user_manual/painting_with_assistants.rst:87
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines."
msgstr ""
"Якщо ви утримуватимете натиснутою клавішу :kbd:`Shift` при визначенні перших "
"двох точок, ці точки буде розташовано на одній вертикалі або горизонталі."

#: ../../user_manual/painting_with_assistants.rst:93
msgid "Spline"
msgstr "Сплайни"

#: ../../user_manual/painting_with_assistants.rst:95
msgid ""
"This assistant allows you to position and adjust four points to create a "
"cubic bezier curve. You can then draw along the curve, snapping your brush "
"stroke directly to the curve line. Perfect curves every time!"
msgstr ""
"У цьому допоміжному засобі вам доведеться розташувати і скоригувати "
"розташування чотирьох точок для створення кубічної кривої Безьє. Далі, ви "
"зможете малювати уздовж кривої, скориставшись прилипанням мазка "
"безпосередньо до лінії кривої. Кожного разу у вас виходитимуть ідеальні "
"лінії!"

#: ../../user_manual/painting_with_assistants.rst:99
msgid ""
"If you press the :kbd:`Shift` key while holding the first two handles, they "
"will snap to perfectly horizontal or vertical lines. Press the :kbd:`Shift` "
"key while holding the third or fourth handle, they will snap relative to the "
"handle they are attached to."
msgstr ""
"Якщо ви утримуватимете натиснутою клавішу :kbd:`Shift` при визначенні перших "
"двох точок, ці точки буде розташовано на одній вертикалі або горизонталі. "
"Утримуйте натиснутою клавішу :kbd:`Shift` під час визначення третьої або "
"четвертої точки, щоб отримати відносне прилипання до відповідної точки."

#: ../../user_manual/painting_with_assistants.rst:107
msgid "Vanishing Point"
msgstr "Точка сходу"

#: ../../user_manual/painting_with_assistants.rst:109
msgid ""
"This assistant allows you to create a vanishing point, typically used for a "
"horizon line. A preview line is drawn and all your snapped lines are drawn "
"to this line."
msgstr ""
"За допомогою цього допоміжного засобу можна створити нескінченно віддалену "
"точку, яка типово використовується для реальної точки на горизонті. Буде "
"намальовано попередньо визначену лінію, а усі відповідні лінії прилипатимуть "
"до неї."

#: ../../user_manual/painting_with_assistants.rst:112
msgid ""
"It is one point, with four helper points to align it to previously created "
"perspective lines."
msgstr ""
"Потрібна одна точка із наступним вирівнюванням чотирьох допоміжних точок за "
"попередньо створеними лініями перспективи."

#: ../../user_manual/painting_with_assistants.rst:115
msgid "They are made and manipulated with the :ref:`assistant_tool`."
msgstr ""
"Ці точки створюються і керуються за допомогою інструмента :ref:"
"`assistant_tool`."

#: ../../user_manual/painting_with_assistants.rst:117
msgid ""
"If you press the :kbd:`Shift` key while holding the center handle, they will "
"snap to perfectly horizontal or vertical lines depending on the position of "
"where it previously was."
msgstr ""
"Якщо утримувати натиснутою клавішу :kbd:`Shift` під час визначення "
"центральної точки, точки прилипатимуть до горизонталі або вертикалі, залежно "
"від попереднього розташування центральної точки."

#: ../../user_manual/painting_with_assistants.rst:123
msgid "The vanishing point assistant also shows several general lines."
msgstr "Допоміжний засіб точки сходу також показу декілька загальних ліній."

#: ../../user_manual/painting_with_assistants.rst:125
msgid ""
"When you've just created, or when you've just moved a vanishing point "
"assistant, it will be selected. This means you can modify the amount of "
"lines shown in the tool options of the :ref:`assistant_tool`."
msgstr ""
"Якщо ви щойно створили або щойно пересунули точку допоміжного засобу точки "
"сходу, точку сходу буде позначено. Це означає, що ви можете змінити "
"кількість ліній, які показуватиме допоміжний засіб, за допомогою параметрів "
"інструмента на панелі :ref:`assistant_tool`."

#: ../../user_manual/painting_with_assistants.rst:130
msgid "Fish Eye Point"
msgstr "Точка «риб’ячого ока»"

#: ../../user_manual/painting_with_assistants.rst:132
msgid ""
"Like the vanishing point assistant, this assistant is per a set of parallel "
"lines in a 3d space. So to use it effectively, use two, where the second is "
"at a 90 degrees angle of the first, and add a vanishing point to the center "
"of both. Or combine one with a parallel ruler and a vanishing point, or even "
"one with two vanishing points. The possibilities are quite large."
msgstr ""
"Подібно до допоміжного засобу точки сходу, цей допоміжний засіб показує "
"набір паралельних ліній у просторі. Отже, для ефективного використання слід "
"додати дві лінії, перпендикулярні одна до одної, а нескінченно віддалену "
"точку додати у центрі кожної з них, на перетині. Або поєднати одну лінію із "
"паралельною лінійкою і нескінченно віддаленою точкою, або навіть створити "
"одну лінію із двома нескінченно віддаленими точками. Варіантів доволі багато."

#: ../../user_manual/painting_with_assistants.rst:139
msgid ""
"This assistant will not just give feedback/snapping between the vanishing "
"points, but also give feedback to the relative left and right of the "
"assistant. This is so you can use it in edge-cases like panoramas with "
"relative ease."
msgstr ""
"У цьому допоміжному засобі поєднується прив'язка-прилипання між нескінченно "
"віддаленими точками та прив'язка до відносних напрямків ліворуч і праворуч "
"від області дії допоміжного засобу. Оте, ви можете відносно просто "
"скористатися ним для екстремальних випадків, зокрема панорам."

#: ../../user_manual/painting_with_assistants.rst:149
msgid "Tutorials"
msgstr "Підручники"

#: ../../user_manual/painting_with_assistants.rst:151
msgid ""
"Check out this in depth discussion and tutorial on https://www.youtube.com/"
"watch?v=OhEv2pw3EuI"
msgstr ""
"Докладне обговорення та підручник можна знайти `тут <https://www.youtube.com/"
"watch?v=OhEv2pw3EuI>`_"

#: ../../user_manual/painting_with_assistants.rst:154
msgid "Technical Drawing"
msgstr "Технічне креслення"

#: ../../user_manual/painting_with_assistants.rst:157
msgid "Setting up Krita for technical drawing-like perspectives"
msgstr ""
"Налаштовування Krita для виконання технічних рисунків, зокрема перспектив"

#: ../../user_manual/painting_with_assistants.rst:159
msgid ""
"So now that you've seen the wide range of drawing assistants that Krita "
"offers, here is an example of how using these assistants you can set up "
"Krita for technical drawing."
msgstr ""
"Тепер, коли ми оглянули широкий діапазон допоміжних засобів малювання, які "
"передбачено у Krita, розглянемо приклад використання цих допоміжних засобів "
"для створення технічного рисунка."

#: ../../user_manual/painting_with_assistants.rst:163
msgid ""
"This tutorial below should give you an idea of how to set up the assistants "
"for specific types of technical views."
msgstr ""
"За допомогою цього підручника ви зможете ознайомитися із тим, як налаштувати "
"допоміжні засоби для специфічних типів технічних рисунків."

#: ../../user_manual/painting_with_assistants.rst:166
msgid ""
"If you want to instead do the true projection, check out :ref:`the "
"projection category <cat_projection>`."
msgstr ""
"Якщо ж вам потрібна істина перспектива, ознайомтеся із вмістом :ref:"
"`категорії розділів щодо проєкцій <cat_projection>`."

#: ../../user_manual/painting_with_assistants.rst:169
msgid "Orthographic"
msgstr "Ортографічна проєкція"

#: ../../user_manual/painting_with_assistants.rst:171
msgid ""
"Orthographic is a mode where you try to look at something from the left or "
"the front. Typically, you try to keep everything in exact scale with each "
"other, unlike perspective deformation."
msgstr ""
"Ортографічна проєкція — режим, у якому ви дивитеся на щось зліва або "
"спереду. Типово, ви намагаєтеся показати усе у однаковому масштабі, на "
"відміну від масштабу, який деформовано перспективою."

#: ../../user_manual/painting_with_assistants.rst:175
msgid ""
"The key assistant you want to use here is the Parallel Ruler. You can set "
"these up horizontally or vertically, so you always have access to a Grid."
msgstr ""
"Ключовим допоміжним засобом при цьому є паралельна лінійка. Ви можете "
"створити горизонтальну або вертикальну лінійку, отже завжди матимете доступ "
"до ґратки."

#: ../../user_manual/painting_with_assistants.rst:180
msgid "Axonometric"
msgstr "Аксонометрична проєкція"

#: ../../user_manual/painting_with_assistants.rst:182
msgid "All of these are set up using three Parallel Rulers."
msgstr "Усе це намальовано за допомогою трьох паралельних лінійок."

#: ../../user_manual/painting_with_assistants.rst:185
msgid ".. image:: images/assistants/Assistants_oblique.png"
msgstr ".. image:: images/assistants/Assistants_oblique.png"

#: ../../user_manual/painting_with_assistants.rst:187
msgid ""
"For oblique, set two parallel rulers to horizontal and vertical, and one to "
"an angle, representing depth."
msgstr ""
"Для косокутної проєкції встановіть дві паралельні лінійки для горизонталі і "
"вертикалі і ще одну лінійку під кутом. Остання лінійка відповідатиме глибині."

#: ../../user_manual/painting_with_assistants.rst:188
msgid "Oblique"
msgstr "Косокутна проєкція"

#: ../../user_manual/painting_with_assistants.rst:191
msgid ".. image:: images/assistants/Assistants_dimetric.png"
msgstr ".. image:: images/assistants/Assistants_dimetric.png"

#: ../../user_manual/painting_with_assistants.rst:193
msgid ""
"Isometric perspective has technically all three rulers set up at 120° from "
"each other. Except when it's game isometric, then it's a type of dimetric "
"projection where the diagonal values are a 116.565° from the main. The "
"latter can be easily set up by snapping the assistants to a grid."
msgstr ""
"Технічно, у ізометрії усі три лінійки розташовано під кутом 120° одна до "
"одної. Виключенням є ігрова ізометрія, яка є типом диметричної проєкції, де "
"діагональні лінійки розташовано під кутом 116,565° до основної. Таку "
"ізометрію можна доволі просто налаштувати, вказавши прилипання допоміжних "
"ліній до ґратки."

#: ../../user_manual/painting_with_assistants.rst:197
msgid "Dimetric & Isometric"
msgstr "Диметрична та ізометрична проєкції"

#: ../../user_manual/painting_with_assistants.rst:200
msgid ".. image:: images/assistants/Assistants_trimetric.png"
msgstr ".. image:: images/assistants/Assistants_trimetric.png"

#: ../../user_manual/painting_with_assistants.rst:202
msgid ""
"Is when all the angles are slightly different. Often looks like a slightly "
"angled isometric."
msgstr ""
"Це, коли усі кути трохи різними. Часто виглядає як ізометрія із дещо "
"зміненими кутами."

#: ../../user_manual/painting_with_assistants.rst:203
msgid "Trimetric"
msgstr "Триметрична проєкція"

#: ../../user_manual/painting_with_assistants.rst:206
msgid "Linear Perspective"
msgstr "Лінійна перспектива"

#: ../../user_manual/painting_with_assistants.rst:209
msgid ".. image:: images/assistants/Assistants_1_point_perspective.png"
msgstr ".. image:: images/assistants/Assistants_1_point_perspective.png"

#: ../../user_manual/painting_with_assistants.rst:211
msgid ""
"A 1 point perspective is set up using 1 vanishing point, and two crossing "
"perpendicular parallel rulers."
msgstr ""
"Одноточкова перспектива, яка налаштовується за допомогою 1 нескінченно "
"віддаленої точки і двох перпендикулярних паралельних лінійок, які "
"схрещуються."

#: ../../user_manual/painting_with_assistants.rst:212
msgid "1 Point Perspective"
msgstr "Одноточкова перспектива"

#: ../../user_manual/painting_with_assistants.rst:215
msgid ".. image:: images/assistants/Assistants_2_point_perspective.png"
msgstr ".. image:: images/assistants/Assistants_2_point_perspective.png"

#: ../../user_manual/painting_with_assistants.rst:217
msgid ""
"A 2 point perspective is set up using 2 vanishing point and 1 vertical "
"parallel ruler. Often, putting the vanishing points outside the frame a "
"little can decrease the strength of it."
msgstr ""
"Двоточкова перспектива, яка налаштовується за допомогою 2 лінійок "
"нескінченно віддаленої точки і 1 вертикальної паралельної лінійки. Часто, "
"розташування нескінченно віддалених точок поза межами кадру може дещо "
"послабити ефект такої перспективи."

#: ../../user_manual/painting_with_assistants.rst:219
msgid "2 Point Perspective"
msgstr "Двоточкова перспектива"

#: ../../user_manual/painting_with_assistants.rst:222
msgid ".. image:: images/assistants/Assistants_2_pointperspective_02.png"
msgstr ".. image:: images/assistants/Assistants_2_pointperspective_02.png"

#: ../../user_manual/painting_with_assistants.rst:224
msgid ".. image:: images/assistants/Assistants_3_point_perspective.png"
msgstr ".. image:: images/assistants/Assistants_3_point_perspective.png"

#: ../../user_manual/painting_with_assistants.rst:226
msgid "3 Point Perspective"
msgstr "Триточкова перспектива"

#: ../../user_manual/painting_with_assistants.rst:226
msgid "A 3 point perspective is set up using 3 vanishing point rulers."
msgstr ""
"Триточкова перспектива, яка налаштовується за допомогою 3 лінійок "
"нескінченно віддаленої точки."

#: ../../user_manual/painting_with_assistants.rst:229
msgid "Logic of the vanishing point"
msgstr "Логіка роботи нескінченно віддаленої точки"

#: ../../user_manual/painting_with_assistants.rst:231
msgid ""
"There's a little secret that perspective tutorials don't always tell you, "
"and that's that a vanishing point is the point where any two parallel lines "
"meet. This means that a 1 point perspective and 2 point perspective are "
"virtually the same."
msgstr ""
"Існує невеличкий секрет, про який не завжди пишуть у підручниках із "
"перспективи. Він полягає у тому, що нескінченно віддалена точка (точка "
"сходу) є точкою, у якій перетинаються будь-які дві паралельні прямі. Це "
"означає, що одноточкова перспектива і двоточкова перспектива віртуально не "
"відрізняються одна від одної."

#: ../../user_manual/painting_with_assistants.rst:233
msgid ""
"We can prove this via a little experiment. That good old problem: drawing a "
"rail-road."
msgstr ""
"Довести це можна за допомогою простого експерименту. Доволі давня проблема: "
"малювання залізничної колії."

#: ../../user_manual/painting_with_assistants.rst:236
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_01.png"
msgstr ".. image:: images/assistants/Assistants_vanishing_point_logic_01.png"

#: ../../user_manual/painting_with_assistants.rst:237
msgid ""
"You are probably familiar with the problem: How to determine where the next "
"beam is going to be, as perspective projection will make them look closer "
"together."
msgstr ""
"Ймовірно, ви знайомі із цією проблемою: як визначити, де малювати наступну "
"шпалу, оскільки проєктування на поле зору призводить до того, що візуально "
"дальні шпали розташовуватимуться усе ближче одна до одної."

#: ../../user_manual/painting_with_assistants.rst:240
msgid ""
"Typically, the solution is to draw a line in the middle and then draw lines "
"diagonally across. After all, those lines are parallel, meaning that the "
"exact same distance is used."
msgstr ""
"Типовим рішенням є малювання лінії посередині колії з наступним малюванням "
"ліній уздовж діагоналей шпал. Оскільки усі ці лінії є паралельними, відстань "
"між об'єктами, яким вони відповідають, лишатиметься однаковою."

#: ../../user_manual/painting_with_assistants.rst:243
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_02.png"
msgstr ".. image:: images/assistants/Assistants_vanishing_point_logic_02.png"

#: ../../user_manual/painting_with_assistants.rst:244
msgid ""
"But because they are parallel, we can use a vanishing point assistant "
"instead, and we use the alignment handles to align it to the diagonal of the "
"beam, and to the horizontal (here marked with red)."
msgstr ""
"Але, оскільки лінії є паралельними, замість цього способу ми можемо "
"скористатися допоміжним засобом точки сходу. Ми використовуватимемо точки "
"вирівнювання для вирівнювання ліній за діагоналлю шпали та за горизонталлю "
"(на нашому малюнку позначено червоним кольором)."

#: ../../user_manual/painting_with_assistants.rst:247
msgid ""
"That diagonal can then in turn be used to determine the position of the "
"beams:"
msgstr ""
"Намальованою діагоналлю можна скористатися для визначення розташування "
"наступної шпали:"

#: ../../user_manual/painting_with_assistants.rst:251
msgid ".. image:: images/assistants/Assistants_vanishing_point_logic_03.png"
msgstr ".. image:: images/assistants/Assistants_vanishing_point_logic_03.png"

#: ../../user_manual/painting_with_assistants.rst:252
msgid ""
"Because any given set of lines has a vanishing point (outside of the ones "
"flat on the view-plane), there can be an infinite amount of vanishing points "
"in a linear perspective. Therefore, Krita allows you to set vanishing points "
"yourself instead of forcing you to only use a few."
msgstr ""
"Оскільки у будь-якого набору паралельних прямих є власна точка сходу (поза "
"їхньою площиною у полі зору), у лінійній перспективі кількість нескінченно "
"віддалених точок може бути нескінченною. Через це у Krita передбачено "
"можливість визначення нескінченно віддалених точок власноруч, без "
"примусового обмеження кількості таких точок певним значенням."

#: ../../user_manual/painting_with_assistants.rst:255
msgid "Fish Eye perspective"
msgstr "Перспектива риб'ячого ока"

#: ../../user_manual/painting_with_assistants.rst:257
msgid ""
"Fish eye perspective works much the same as the linear perspective, the big "
"difference being that in a fish-eye perspective, any parallel set of lines "
"has two vanishing points, each for one side."
msgstr ""
"Перспектива риб'ячого ока працює подібно до лінійної перспективи. Втім, "
"великою відмінністю є те, що у перспективі риб'ячого ока будь-який набір "
"паралельних прямих має дві точки сходу, по одній з кожного боку від центру."

#: ../../user_manual/painting_with_assistants.rst:261
msgid ""
"So, to set them up, the easiest way is one horizontal, one vertical, on the "
"same spot, and one vanishing point assistant in the middle."
msgstr ""
"Отже, для налаштовування перспективи у одній області потрібна одна "
"горизонтальна допоміжна лінія, одна вертикальна і одна допоміжна нескінченно "
"віддалена точка посередині."

#: ../../user_manual/painting_with_assistants.rst:265
msgid ".. image:: images/assistants/Fish-eye.gif"
msgstr ".. image:: images/assistants/Fish-eye.gif"

#: ../../user_manual/painting_with_assistants.rst:266
msgid ""
"But, you can also make one horizontal one that is just as big as the other "
"horizontal one, and put it halfway:"
msgstr ""
"Втім, ви також можете створити одну горизонтальну допоміжну лінію такого "
"самого розміру, як іще одна горизонтальна, і розташувати їх ось так:"
