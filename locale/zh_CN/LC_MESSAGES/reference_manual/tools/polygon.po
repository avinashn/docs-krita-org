msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___polygon.pot\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:30
msgid ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"
msgstr ""
".. image:: images/icons/polygon_tool.svg\n"
"   :alt: toolpolygon"

#: ../../reference_manual/tools/polygon.rst:1
msgid "Krita's polygon tool reference."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:10
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:10
msgid "Polygon"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:15
msgid "Polygon Tool"
msgstr "多边形工具"

#: ../../reference_manual/tools/polygon.rst:17
msgid "|toolpolygon|"
msgstr ""

#: ../../reference_manual/tools/polygon.rst:19
msgid ""
"With this tool you can draw polygons. Click the |mouseleft| to indicate the "
"starting point and successive vertices, then double-click or press the :kbd:"
"`Enter` key to connect the last vertex to the starting point."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:21
msgid ":kbd:`Shift + Z` undoes the last clicked point."
msgstr ""

#: ../../reference_manual/tools/polygon.rst:24
msgid "Tool Options"
msgstr "工具选项"
