# Spanish translations for docs_krita_org_reference_manual___tools___perspective_grid.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___perspective_grid\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-07-14 13:39+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../<rst_epilog>:50
msgid ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"
msgstr ""
".. image:: images/icons/perspectivegrid_tool.svg\n"
"   :alt: toolperspectivegrid"

#: ../../reference_manual/tools/perspective_grid.rst:1
msgid "Krita's perspective grid tool reference."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Tools"
msgstr "Herramientas"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Perspective"
msgstr "Perspectiva"

#: ../../reference_manual/tools/perspective_grid.rst:11
msgid "Grid"
msgstr "Cuadrícula"

#: ../../reference_manual/tools/perspective_grid.rst:16
msgid "Perspective Grid Tool"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:18
msgid "|toolperspectivegrid|"
msgstr "|toolperspectivegrid|"

#: ../../reference_manual/tools/perspective_grid.rst:22
msgid "Deprecated in 3.0, use the :ref:`assistant_perspective` instead."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:24
msgid ""
"The perspective grid tool allows you to draw and manipulate grids on the "
"canvas that can serve as perspective guides for your painting. A grid can be "
"added to your canvas by first clicking the tool in the tool bar and then "
"clicking four points on the canvas which will serve as the four corners of "
"your grid."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:27
msgid ".. image:: images/tools/Perspectivegrid.png"
msgstr ".. image:: images/tools/Perspectivegrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:28
msgid ""
"The grid can be manipulated by pulling on any of its four corners. The grid "
"can be extended by clicking and dragging a midpoint of one of its edges. "
"This will allow you to expand the grid at other angles. This process can be "
"repeated on any subsequent grid or grid section. You can join the corners of "
"two grids by dragging one onto the other. Once they are joined they will "
"always move together, they cannot be separated. You can delete any grid by "
"clicking on the red X at its center. This tool can be used to build "
"reference for complex scenes."
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:30
msgid "As displayed while the Perspective Grid tool is active: *"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:33
msgid ".. image:: images/tools/Multigrid.png"
msgstr ".. image:: images/tools/Multigrid.png"

#: ../../reference_manual/tools/perspective_grid.rst:34
msgid "As displayed while any other tool is active: *"
msgstr ""

#: ../../reference_manual/tools/perspective_grid.rst:37
msgid ".. image:: images/tools/KritaPersgridnoedit.png"
msgstr ".. image:: images/tools/KritaPersgridnoedit.png"

#: ../../reference_manual/tools/perspective_grid.rst:38
msgid ""
"You can toggle the visibility of the grid from the main menu :menuselection:"
"`View --> Show Perspective Grid` option. You can also clear any grid setup "
"you have and start over by using the :menuselection:`View --> Clear "
"Perspective Grid`."
msgstr ""
