# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-08 17:35+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: images image en KritaDigitalColorMixerDocker dockers\n"

#: ../../reference_manual/dockers/digital_color_mixer.rst:1
msgid "Overview of the digital color mixer docker."
msgstr "Introdução à área de mistura digital de cores."

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
#: ../../reference_manual/dockers/digital_color_mixer.rst:16
msgid "Digital Color Mixer"
msgstr "Mistura Digital de Cores"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Mixing"
msgstr "Mistura de Cores"

#: ../../reference_manual/dockers/digital_color_mixer.rst:11
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/dockers/digital_color_mixer.rst:19
msgid ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"
msgstr ".. image:: images/dockers/Krita_Digital_Color_Mixer_Docker.png"

#: ../../reference_manual/dockers/digital_color_mixer.rst:20
msgid "This docker allows you to do simple mathematical color mixing."
msgstr ""
"Esta área permite-lhe efectuar algumas misturas de cores com operações "
"matemáticas.."

#: ../../reference_manual/dockers/digital_color_mixer.rst:22
msgid "It works as follows:"
msgstr "Funciona da seguinte forma:"

#: ../../reference_manual/dockers/digital_color_mixer.rst:24
msgid "You have on the left side the current color."
msgstr "Tem do lado esquerdo a cor actual."

#: ../../reference_manual/dockers/digital_color_mixer.rst:26
msgid ""
"Next to that there are six columns. Each of these columns consists of three "
"rows: The lowest row is the color that you are mixing the current color "
"with. Ticking this button allows you to set a different color using a "
"palette and the mini-color wheel. The slider above this mixing color "
"represent the proportions of the mixing color and the current color. The "
"higher the slider, the less of the mixing color will be used in mixing. "
"Finally, the result color. Clicking this will change your current color to "
"the result color."
msgstr ""
"A seguir a ela encontram-se seis colunas. Cada uma dessas colunas consiste "
"em três linhas: A linha inferior é a cor com que está a misturar a cor "
"actual. Se carregar neste botão, poderá escolher uma cor diferente com uma "
"paleta e a mini-roda de cores. A barra acima desta cor de mistura representa "
"as proporções da cor de mistura e da cor actual. Quanto mais elevada a "
"barra, será usada uma menor parte da cor de mistura. Finalmente, a cor "
"resultante. Se carregar nesta, irá mudar a sua cor actual para a cor "
"resultante."
