# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:19+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: toolrectangle icons Krita image kbd rectangletool\n"
"X-POFile-SpellExtra: images alt Kritamouseleft mouseleft\n"

#: ../../<generated>:1
msgid "Ratio"
msgstr "Taxa"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: botão esquerdo"

#: ../../<rst_epilog>:26
msgid ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: toolrectangle"
msgstr ""
".. image:: images/icons/rectangle_tool.svg\n"
"   :alt: ferramenta do rectângulo"

#: ../../reference_manual/tools/rectangle.rst:1
msgid "Krita's rectangle tool reference."
msgstr "A referência da ferramenta de rectângulos do Krita."

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Tools"
msgstr "Ferramentas"

#: ../../reference_manual/tools/rectangle.rst:10
msgid "Rectangle"
msgstr "Rectângulo"

#: ../../reference_manual/tools/rectangle.rst:15
msgid "Rectangle Tool"
msgstr "Ferramenta de Rectângulo"

#: ../../reference_manual/tools/rectangle.rst:17
msgid "|toolrectangle|"
msgstr "|toolrectangle|"

#: ../../reference_manual/tools/rectangle.rst:19
msgid ""
"This tool can be used to paint rectangles, or create rectangle shapes on a "
"vector layer. Click and hold |mouseleft| to indicate one corner of the "
"rectangle, drag to the opposite corner, and release the button."
msgstr ""
"Esta ferramenta pode ser usada para pintar ou criar formas rectangulares "
"sobre uma camada vectorial. Carregue e mantenha pressionado o |mouseleft| "
"para indicar um canto do rectângulo, arraste para o canto oposto e largue o "
"botão."

#: ../../reference_manual/tools/rectangle.rst:22
msgid "Hotkeys and Sticky-keys"
msgstr "Atalhos e Teclas Fixas"

#: ../../reference_manual/tools/rectangle.rst:24
msgid "There's no default hotkey for switching to rectangle."
msgstr ""
"Não existe nenhuma combinação de teclas predefinida para mudar para o "
"rectângulo."

#: ../../reference_manual/tools/rectangle.rst:26
msgid ""
"If you hold the :kbd:`Shift` key while drawing, a square will be drawn "
"instead of a rectangle. Holding the :kbd:`Ctrl` key will change the way the "
"rectangle is constructed. Normally, the first mouse click indicates one "
"corner and the second click the opposite. With the :kbd:`Ctrl` key, the "
"initial mouse position indicates the center of the rectangle, and the final "
"mouse position indicates a corner. You can press the :kbd:`Alt` key while "
"still keeping |mouseleft| down to move the rectangle to a different location."
msgstr ""
"Se mantiver carregado o :kbd:`Shift` enquanto arrasta, será desenhado um "
"quadrado em vez de um rectângulo. Se mantiver carregado o :kbd:`Ctrl`, irá "
"mudar a forma como é construído o rectângulo. Normalmente, o primeiro "
"'click' do rato define um canto e o segundo define o canto oposto. Com o :"
"kbd:`Ctrl`, a posição inicial do rato indica o centro do rectângulo e a "
"posição final indica um canto. Poderá carregar no :kbd:`Alt` enquanto mantém "
"pressionado o |mouseleft| para mover o rectângulo para um local diferente."

#: ../../reference_manual/tools/rectangle.rst:28
msgid ""
"You can change between the corner/corner and center/corner drawing methods "
"as often as you want by pressing or releasing the :kbd:`Ctrl` key, provided "
"that you keep |mouseleft| pressed. With the :kbd:`Ctrl` key pressed, mouse "
"movements will affect all four corners of the rectangle (relative to the "
"center), without the :kbd:`Ctrl` key, one of the corners is unaffected."
msgstr ""
"Poderá alternar entre os métodos de desenho canto/canto e centro/canto as "
"vezes que desejar, carregando ou largando o :kbd:`Ctrl`, desde que mantenha "
"carregado o |mouseleft|. Com o :kbd:`Ctrl` carregado, os movimentos irão "
"afectar todos os quatro cantos do rectângulo (em relação ao centro); sem o :"
"kbd:`Ctrl`, um dos cantos não será afectado."

#: ../../reference_manual/tools/rectangle.rst:32
msgid "Tool Options"
msgstr "Opções da Ferramenta"

#: ../../reference_manual/tools/rectangle.rst:35
msgid "Fill"
msgstr "Preencher"

#: ../../reference_manual/tools/rectangle.rst:37
msgid "Not filled"
msgstr "Não preenchido"

#: ../../reference_manual/tools/rectangle.rst:38
msgid "The rectangle will be transparent from the inside."
msgstr "O rectângulo ficará transparente na parte interior."

#: ../../reference_manual/tools/rectangle.rst:39
msgid "Foreground color"
msgstr "Cor do texto"

#: ../../reference_manual/tools/rectangle.rst:40
msgid "The rectangle will use the foreground color as fill."
msgstr "O rectângulo irá usar a cor principal como preenchimento."

#: ../../reference_manual/tools/rectangle.rst:41
msgid "Background color"
msgstr "Cor de fundo"

#: ../../reference_manual/tools/rectangle.rst:42
msgid "The rectangle will use the background color as fill."
msgstr "O rectângulo irá usar a cor de fundo como preenchimento."

#: ../../reference_manual/tools/rectangle.rst:44
msgid "Pattern"
msgstr "Padrão"

#: ../../reference_manual/tools/rectangle.rst:44
msgid "The rectangle will use the active pattern as fill."
msgstr "O rectângulo irá usar o padrão activo como preenchimento."

#: ../../reference_manual/tools/rectangle.rst:47
msgid "Outline"
msgstr "Destaque"

#: ../../reference_manual/tools/rectangle.rst:49
msgid "No Outline"
msgstr "Sem Contorno"

#: ../../reference_manual/tools/rectangle.rst:50
msgid "The Rectangle will render without outline."
msgstr "O rectângulo será desenhado sem qualquer contorno."

#: ../../reference_manual/tools/rectangle.rst:52
msgid "Brush"
msgstr "Pincel"

#: ../../reference_manual/tools/rectangle.rst:52
msgid "The Rectangle will use the current selected brush to outline."
msgstr "O Rectângulo irá usar o pincel seleccionado de momento no contorno."

#: ../../reference_manual/tools/rectangle.rst:55
msgid ""
"On vector layers, the rectangle will not render with a brush outline, but "
"rather a vector outline."
msgstr ""
"Nas camadas vectoriais, o rectângulo não será desenhado com o contorno do "
"pincel, mas sim com um contorno vectorial."

#: ../../reference_manual/tools/rectangle.rst:57
msgid "Anti-aliasing"
msgstr "Suavização"

#: ../../reference_manual/tools/rectangle.rst:58
msgid ""
"This toggles whether or not to give selections feathered edges. Some people "
"prefer hard-jagged edges for their selections."
msgstr ""
"Isto indica se são usados contornos leves nas selecções. Algumas pessoas "
"preferem arestas vincadas para as suas selecções."

#: ../../reference_manual/tools/rectangle.rst:59
msgid "Width"
msgstr "Largura"

#: ../../reference_manual/tools/rectangle.rst:60
msgid ""
"Gives the current width. Use the lock to force the next selection made to "
"this width."
msgstr ""
"Indica a largura actual. Use o cadeado para forçar a próxima selecção a ter "
"esta largura."

#: ../../reference_manual/tools/rectangle.rst:61
msgid "Height"
msgstr "Altura"

#: ../../reference_manual/tools/rectangle.rst:62
msgid ""
"Gives the current height. Use the lock to force the next selection made to "
"this height."
msgstr ""
"Indica a altura actual. Use o cadeado para forçar a próxima selecção a ter "
"esta altura."

#: ../../reference_manual/tools/rectangle.rst:66
msgid ""
"Gives the current ratio. Use the lock to force the next selection made to "
"this ratio."
msgstr ""
"Indica a proporção de tamanho actual. Use o cadeado para forçar a próxima "
"selecção a manter estas proporções."

#: ../../reference_manual/tools/rectangle.rst:68
msgid "Round X"
msgstr "Arredondamento em X"

#: ../../reference_manual/tools/rectangle.rst:70
msgid "The horizontal radius of the rectangle corners."
msgstr "O raio horizontal dos cantos rectangulares."

#: ../../reference_manual/tools/rectangle.rst:72
msgid "Round Y"
msgstr "Arredondamento em Y"

#: ../../reference_manual/tools/rectangle.rst:74
msgid "The vertical radius of the rectangle corners."
msgstr "O raio vertical dos cantos rectangulares."
