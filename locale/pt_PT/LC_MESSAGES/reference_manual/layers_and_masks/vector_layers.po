# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:33+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: vectorgraphics en image Transform kbd images\n"
"X-POFile-SpellExtra: Rotatevector Vectorlayer Strokeandfill program ref\n"
"X-POFile-SpellExtra: Skew Resize Vectorguides Krita Pointcurvemanip\n"
"X-POFile-SpellExtra: Strokeprops Inkscape Strokeandfillstroke Fillrulenon\n"
"X-POFile-SpellExtra: odd Fillruleeven\n"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
msgid ".. image:: images/vector/Fill_rule_even-odd.svg"
msgstr ".. image:: images/vector/Fill_rule_even-odd.svg"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:None
msgid ".. image:: images/vector/Fill_rule_non-zero.svg"
msgstr ".. image:: images/vector/Fill_rule_non-zero.svg"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:1
msgid "How to use vector layers in Krita."
msgstr "Como usar as camadas vectoriais no Krita."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
msgid "Vector"
msgstr "Vector"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:15
msgid "Layers"
msgstr "Camadas"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:20
msgid "Vector Layers"
msgstr "Camadas Vectoriais"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:24
msgid ""
"This page is outdated. Check :ref:`vector_graphics` for a better overview."
msgstr ""
"Esta página está desactualizada. Veja uma melhor introdução em :ref:"
"`vector_graphics`."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:27
msgid "What is a Vector Layer?"
msgstr "O que é uma Camada Vectorial?"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:29
msgid ""
"A Vector Layers, also known as a shape layer, is a type of layers that "
"contains only vector elements."
msgstr ""
"Uma Camada Vectorial, também conhecida por camada de formas, é um tipo de "
"camada que só contém elementos vectoriais."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:31
msgid ""
"This is how vector layers will appear in the :program:`Krita` Layers docker."
msgstr ""
"Isto é como as camadas vectoriais irão aparecer na área de Camadas do :"
"program:`Krita`."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:34
msgid ".. image:: images/vector/Vectorlayer.png"
msgstr ".. image:: images/vector/Vectorlayer.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:35
msgid ""
"It shows the vector contents of the layer on the left side. The icon showing "
"the page with the red bookmark denotes that it is a vector layer. To the "
"right of that is the layer name. Next are the layer visibility and "
"accessibility icons. Clicking the \"eye\" will toggle visibility. Clicking "
"the lock into a closed position will lock the content and editing will no "
"longer be allowed until it is clicked again and the lock on the layer is "
"released."
msgstr ""
"Mostra o conteúdo vectorial da camada do lado esquerdo. O ícone que mostra a "
"página com o marcador vermelho recorda que é uma camada vectorial. À direita "
"da mesma aparece o nome da camada. De seguida, encontram-se os ícones de "
"visibilidade e acessibilidade da camada. Se carregar no \"olho\", irá "
"comutar a visibilidade. Se carregar no ícone do cadeado para ele ficar "
"fechado, o mesmo ficará com o conteúdo bloqueado, não sendo mais permitido "
"editá-lo a menos que carregue nele de novo e se abra o cadeado."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:38
msgid "Creating a vector layer"
msgstr "Criar uma camada vectorial"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:40
msgid ""
"You can create a vector layer in two ways. Using the extra options from the "
"\"Add Layer\" button you can click the \"Vector Layer\" item and it will "
"create a new vector layer. You can also drag a rectangle or ellipse from the "
"**Add shape** dock onto an active Paint Layer.  If the active layer is a "
"Vector Layer then the shape will be added directly to it."
msgstr ""
"Poderá criar uma camada vectorial de duas formas. Se usar as opções extra do "
"botão \"Adicionar uma Camada\", poderá carregar no item \"Camada Vectorial\" "
"para que ele crie uma nova camada vectorial. Poderá também arrastar um "
"rectângulo ou elipse na área para **Adicionar uma forma** sobre uma Camada "
"de Pintura activa. Se a camada activa for uma Camada Vectorial, então a "
"forma será adicionada directamente a ela."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:43
msgid "Editing Shapes on a Vector Layer"
msgstr "Editar as Formas numa Camada Vectorial"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:47
msgid ""
"There's currently a bug with the vector layers that they will always "
"consider themselves to be at 72dpi, regardless of the actual pixel-size. "
"This can make manipulating shapes a little difficult, as the precise input "
"will not allow cm or inch, even though the vector layer coordinate system "
"uses those as a basis."
msgstr ""
"Existe de momento um erro com as camadas vectoriais, nas quais serão sempre "
"consideradas como tendo 72ppp, independentemente do tamanho actual dos "
"pixels. Isto poderá dificultar um pouco a manipulação das formas, dado que a "
"introdução dos dados não irá permitir 'cm' ou 'polegadas', mesmo que o "
"sistema de coordenadas da camada vectorial as suporte de base."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:50
msgid "Basic Shape Manipulation"
msgstr "Manipulação Básica de Formas"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:52
msgid ""
"To edit the shape and colors of your vector element, you will need to use "
"the basic shape manipulation tool."
msgstr ""
"Para editar a forma e as cores do seu elemento vectorial, terá de usar a "
"ferramenta de manipulação de formas básicas."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:54
msgid ""
"Once you have selected this tool, click on the element you want to "
"manipulate and you will see guides appear around your shape."
msgstr ""
"Assim que tiver seleccionado esta ferramenta, carregue no elemento que "
"deseja manipular para ver as guias a aparecer à volta da sua forma."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:57
msgid ".. image:: images/vector/Vectorguides.png"
msgstr ".. image:: images/vector/Vectorguides.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:58
msgid ""
"There are four ways to manipulate your image using this tool and the guides "
"on your shape."
msgstr ""
"Existem quatro formas de manipular a sua imagem com esta ferramenta e as "
"guias na sua forma."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:61
msgid "Transform/Move"
msgstr "Transformar/Mover"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:64
msgid ".. image:: images/vector/Transform.png"
msgstr ".. image:: images/vector/Transform.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:65
msgid ""
"This feature of the tool allows you to move your object by clicking and "
"dragging your shape around the canvas. Holding the :kbd:`Ctrl` key will lock "
"your moves to one axis."
msgstr ""
"Esta funcionalidade da ferramenta permite-lhe mover o seu objecto se "
"carregar e arrastar a sua forma em torno da área de desenho. Se carregar em :"
"kbd:`Ctrl` para bloquear o seu movimento num único eixo."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:68
msgid "Size/Stretch"
msgstr "Dimensionar/Esticar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:71
msgid ".. image:: images/vector/Resize.png"
msgstr ".. image:: images/vector/Resize.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:72
msgid ""
"This feature of the tool allows you to stretch your shape.  Selecting a "
"midpoint will allow stretching along one axis. Selecting a corner point will "
"allow stretching across both axis. Holding the :kbd:`Shift` key will allow "
"you to scale your object. Holding the :kbd:`Ctrl` key will cause your "
"manipulation to be mirrored across your object."
msgstr ""
"Esta característica da ferramenta permite-lhe esticar a sua forma. Se "
"seleccionar um ponto intermédio, poderá esticar ao longo de um eixo. Se "
"seleccionar o ponto de um canto, poderá esticar ao longo de ambos os eixos. "
"Se mantiver carregado o :kbd:`Shift`, poderá ajustar a escala do seu "
"objecto. Se mantiver carregado o :kbd:`Ctrl`, fará com que a sua manipulação "
"seja espelhada ao longo do seu objecto."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:75
msgid "Rotate"
msgstr "Rodar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:78
msgid ".. image:: images/vector/Rotatevector.png"
msgstr ".. image:: images/vector/Rotatevector.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:79
msgid ""
"This feature of the tool will allow you to rotate your object around its "
"center. Holding the :kbd:`Ctrl` key will cause your rotation to lock to 45 "
"degree angles."
msgstr ""
"Esta funcionalidade da ferramenta permitir-lhe-á rodar o seu objecto em "
"torno do seu centro. Se mantiver carregado o :kbd:`Ctrl`, fará com que a sua "
"rotação seja efectuada em saltos de 45 graus."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:82
msgid "Skew"
msgstr "Inclinar"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:85
msgid ".. image:: images/vector/Skew.png"
msgstr ".. image:: images/vector/Skew.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:86
msgid "This feature of the tool will allow you to skew your object."
msgstr ""
"Esta funcionalidade da ferramenta permitir-lhe-á inclinar o seu objecto."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:90
msgid ""
"At the moment there is no way to scale only one side of your vector object. "
"The developers are aware that this could be useful and will work on it as "
"manpower allows."
msgstr ""
"De momento, não existe nenhuma forma de ajustar a escala apenas de um lado "
"do seu objecto vectorial. Os programadores estão cientes da utilidade disto "
"e irão trabalhar sobre o assunto, assim que haja disponibilidade de tempo."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:93
msgid "Point and Curve Shape Manipulation"
msgstr "Manipulação de Formas em Pontos e Curvas"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:95
msgid ""
"Double-click on a vector object to edit the specific points or curves which "
"make up the shape. Click and drag a point to move it around the canvas. "
"Click and drag along a line to curve it between two points. Holding the :kbd:"
"`Ctrl` key will lock your moves to one axis."
msgstr ""
"Faça duplo-click sobre um objecto vectorial para editar os pontos ou curvas "
"específicos que compõem a forma. Carregue e arraste um ponto para o mover "
"por toda a área de desenho. Carregue e arraste ao longo de uma linha para a "
"curvar entre dois pontos. Se mantiver o :kbd:`Ctrl` carregado, irá bloquear "
"os seus movimentos a apenas um eixo."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:98
msgid ".. image:: images/vector/Pointcurvemanip.png"
msgstr ".. image:: images/vector/Pointcurvemanip.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:100
msgid "Stroke and Fill"
msgstr "Traço e Preenchimento"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:102
msgid ""
"In addition to being defined by points and curves, a shape also has two "
"defining properties: **Fill** and **Stroke**. **Fill** defines the color, "
"gradient, or pattern that fills the space inside of the shape object. "
"'**Stroke**' defines the color, gradient, pattern, and thickness of the "
"border along the edge of the shape. These two can be edited using the "
"**Stroke and Fill** dock. The dock has two modes. One for stroke and one for "
"fill. You can change modes by clicking in the dock on the filled square or "
"the black line. The active mode will be shown by which is on top of the "
"other."
msgstr ""
"Para além de ser definida por pontos e curvas, uma forma também tem duas "
"propriedades que a definem: o **Preenchimento** e o **Traço**. O "
"**Preenchimento** define a cor, o gradiente ou o padrão que preenche o "
"espaço dentro do objecto da forma. O **Traço** define a cor, o gradiente, o "
"padrão e a espessura do contorno ao longo da aresta da forma. Estes dois "
"poderão ser editados com a área de **Traço e Preenchimento**. A área tem "
"dois modos - um para o traço e outro para o preenchimento. Poderá mudar os "
"modos se carregar na área acoplável, no quadrado preenchido ou na linha "
"preta. O modo activo será apresentado por cima do mesmo."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:104
msgid ""
"Here is the dock with the fill element active. Notice the red line across "
"the solid white square. This tells us that there is no fill assigned "
"therefore the inside of the shape will be transparent."
msgstr ""
"Aqui está a área acoplável com o elemento de preenchimento activo. Repare na "
"linha vermelha ao longo do quadrado branco. Isto diz-nos que não existe "
"nenhum preenchimento atribuído, como tal o interior da forma ficará "
"transparente."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:107
#: ../../reference_manual/layers_and_masks/vector_layers.rst:129
msgid ".. image:: images/vector/Strokeandfill.png"
msgstr ".. image:: images/vector/Strokeandfill.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:108
msgid "Here is the dock with the stroke element active."
msgstr "Aqui está a área com o elemento do traço activo."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:111
msgid ".. image:: images/vector/Strokeandfillstroke.png"
msgstr ".. image:: images/vector/Strokeandfillstroke.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:113
msgid "Editing Stroke Properties"
msgstr "Editar as Propriedades do Traço"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:115
msgid ""
"The stroke properties dock will allow you to edit a different aspect of how "
"the outline of your vector shape looks."
msgstr ""
"A área de propriedades do traço permitir-lhe-á editar um aspecto diferente "
"de como ficará o contorno da sua forma vectorial."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:118
msgid ".. image:: images/vector/Strokeprops.png"
msgstr ".. image:: images/vector/Strokeprops.png"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:119
msgid ""
"The style selector allows you to choose different patterns and line styles. "
"The width option changes the thickness of the outline on your vector shape. "
"The cap option changes how line endings appear. The join option changes how "
"corners appear."
msgstr ""
"O selector do estilo permite-lhe escolher diferentes padrões e estilos de "
"linha. A opção da espessura muda a largura do contorno da sua forma "
"vectorial. A opção dos extremos definem como terminam as linhas. A opção de "
"junção define como aparecem os cantos."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:121
msgid ""
"The Miter limit controls how harsh the corners of your object will display. "
"The higher the number the more the corners will be allowed to stretch out "
"past the points. Lower numbers will restrict the stroke to shorter and less "
"sharp corners."
msgstr ""
"O limite da Cunha controla o afiamento com que os cantos irão aparecer. "
"Quanto maior o número mais os cantos se poderão esticar para lá dos pontos. "
"Os números mais baixos irão restringir o traço a cantos mais curtos e menos "
"definidos."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:124
msgid "Editing Fill Properties"
msgstr "Editar as Propriedades de Preenchimento"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:126
msgid ""
"All of the fill properties are contained in the **Stroke and Fill** dock."
msgstr ""
"Todas as propriedades de preenchimento constam na área de **Traços e "
"Preenchimento**."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:130
msgid ""
"The large red **X** button will set the fill to none causing the area inside "
"of the vector shape to be transparent."
msgstr ""
"O botão grande vermelho **X** irá configurar o preenchimento como "
"inexistente, fazendo com que a área dentro da forma vectorial fique "
"transparente."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:132
msgid ""
"To the right of that is the solid square. This sets the fill to be a solid "
"color which is displayed in the long button and can be selected by pressing "
"the arrow just to the right of the long button. To the right of the solid "
"square is the gradient button. This will set the fill to display as a "
"gradient. A gradient can be selected by pressing the down arrow next to the "
"long button."
msgstr ""
"À direita disto aparece um quadrado preenchido. Isto indica que o "
"preenchimento será uma cor sólida que aparece no botão comprido e que poderá "
"seleccionado se carregar na seta à direita do botão comprido. À direita do "
"quadrado cheio existe o botão do gradiente. Isto irá configurar o "
"preenchimento para aparecer como um gradiente. Poderá seleccionar um "
"gradiente se carregar na seta para baixo a seguir ao botão comprido."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:134
msgid ""
"Under the **X** is a button that shows a pattern. This inside area will be "
"filled with a pattern. A pattern can be chosen by pressing the arrows next "
"to the long button. The two other buttons are for **fill rules**: the way a "
"self-overlapping path is filled."
msgstr ""
"Debaixo do **X** existe um botão que mostra um padrão. Esta área interior "
"será preenchida com um padrão. Poderá escolher um padrão se carregar nas "
"setas a seguir ao botão comprido. Os outros dois botões servem para as "
"**regras de preenchimento**: a forma como é preenchida uma área auto-"
"sobreposta."

#: ../../reference_manual/layers_and_masks/vector_layers.rst:136
msgid ""
"The button with the inner square blank toggles even-odd mode, where every "
"filled region of the path is next to an unfilled one, like this:"
msgstr ""
"O botão com o quadrado interno activa o modo par-ímpar, onde cada região "
"preenchida do caminho fica a seguir a uma não-preenchida, como a seguinte:"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:142
msgid ""
"The button with the inner square filled toggles non zero mode, where most of "
"the time a self overlapping path is entirely filled except when it overlaps "
"with a sub-path of a different direction that 'decrease the level of "
"overlapping' so that the region between the two is considered outside the "
"path and remain unfilled, like this:"
msgstr ""
"O botão com o quadrado inteiro preenchido activa o modo não-zero onde, na "
"maior parte dos casos, um caminho auto-sobreposto fica completamente "
"preenchido, excepto quando se sobrepõe a um sub-caminho numa direcção "
"diferente que 'diminui' o nível de sobreposição', de forma que a região "
"entre os dois seja considerada fora do caminho e fique sem preenchimento, "
"como se segue:"

#: ../../reference_manual/layers_and_masks/vector_layers.rst:148
msgid ""
"For more (and better) information about fill rules check the `Inkscape "
"manual <http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Attributes-Fill-Stroke."
"html#Attributes-Fill-Rule>`_."
msgstr ""
"Para mais (e melhores) informações sobre as regras de preenchimento, veja o "
"`manual do Inkscape <http://tavmjong.free.fr/INKSCAPE/MANUAL/html/Attributes-"
"Fill-Stroke.html#Attributes-Fill-Rule>`_."
