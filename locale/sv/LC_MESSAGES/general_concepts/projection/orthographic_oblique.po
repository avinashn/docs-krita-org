# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-02 12:05+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_01.svg"
msgstr ".. image:: images/category_projection/projection-cube_01.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_02.svg"
msgstr ".. image:: images/category_projection/projection-cube_02.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_03.svg"
msgstr ".. image:: images/category_projection/projection-cube_03.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_04.svg"
msgstr ".. image:: images/category_projection/projection-cube_04.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_05.svg"
msgstr ".. image:: images/category_projection/projection-cube_05.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection-cube_06.svg"
msgstr ".. image:: images/category_projection/projection-cube_06.svg"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_01.png"
msgstr ".. image:: images/category_projection/projection_image_01.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_02.png"
msgstr ".. image:: images/category_projection/projection_image_02.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_03.png"
msgstr ".. image:: images/category_projection/projection_image_03.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_04.png"
msgstr ".. image:: images/category_projection/projection_image_04.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_05.png"
msgstr ".. image:: images/category_projection/projection_image_05.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_06.png"
msgstr ".. image:: images/category_projection/projection_image_06.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_07.png"
msgstr ".. image:: images/category_projection/projection_image_07.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_08.png"
msgstr ".. image:: images/category_projection/projection_image_08.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_09.png"
msgstr ".. image:: images/category_projection/projection_image_09.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_10.png"
msgstr ".. image:: images/category_projection/projection_image_10.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_11.png"
msgstr ".. image:: images/category_projection/projection_image_11.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_12.png"
msgstr ".. image:: images/category_projection/projection_image_12.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_13.png"
msgstr ".. image:: images/category_projection/projection_image_13.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_image_14.png"
msgstr ".. image:: images/category_projection/projection_image_14.png"

#: ../../general_concepts/projection/orthographic_oblique.rst:None
msgid ".. image:: images/category_projection/projection_animation_01.gif"
msgstr ".. image:: images/category_projection/projection_animation_01.gif"

#: ../../general_concepts/projection/orthographic_oblique.rst:1
msgid "Orthographics and oblique projection."
msgstr "Ortografisk och oblik projektion."

#: ../../general_concepts/projection/orthographic_oblique.rst:10
msgid "So let's start with the basics..."
msgstr "Så låt oss börja med grunderna ..."

#: ../../general_concepts/projection/orthographic_oblique.rst:12
#: ../../general_concepts/projection/orthographic_oblique.rst:16
msgid "Orthographic"
msgstr "Ortografisk"

#: ../../general_concepts/projection/orthographic_oblique.rst:12
msgid "Projection"
msgstr "Projektion"

#: ../../general_concepts/projection/orthographic_oblique.rst:18
msgid ""
"Despite the fancy name, you probably know what orthographic is. It is a "
"schematic representation of an object, draw undeformed. Like the following "
"example:"
msgstr ""
"Trots det tjusiga namnet, vet du troligen vad ortografisk är. Det är en "
"schematisk representation av ett objekt, ritat utan deformering. Som "
"följande exempel:"

#: ../../general_concepts/projection/orthographic_oblique.rst:23
msgid ""
"This is a rectangle. We have a front, top and side view. Put into "
"perspective it should look somewhat like this:"
msgstr ""
"Det här är en rektangel. Vi har en framsida, ovansida och sidovy. Med "
"perspektiv ska den se ut ungefär så här:"

#: ../../general_concepts/projection/orthographic_oblique.rst:28
msgid ""
"While orthographic representations are kinda boring, they're also a good "
"basis to start with when you find yourself in trouble with a pose. But we'll "
"get to that in a bit."
msgstr ""
"Även om ortografiska representationer är lite tråkiga, så är de också en bra "
"grund att börja med när man har problem med en pose. Men vi kommer dit om "
"ett tag."

#: ../../general_concepts/projection/orthographic_oblique.rst:33
msgid "Oblique"
msgstr "Oblik"

#: ../../general_concepts/projection/orthographic_oblique.rst:35
msgid ""
"So, if we can say that the front view is the viewer looking at the front, "
"and the side view is the viewer directly looking at the side. (The "
"perpendicular line being the view plane it is projected on)"
msgstr ""
"Så om vi kan säga att framsidan är betraktaren som tittar framifrån, och "
"sidovyn är betraktaren som tittar direkt på sidan (Den vinkelrätta linjen är "
"vyplanet som den projiceras på)."

#: ../../general_concepts/projection/orthographic_oblique.rst:40
msgid "Then we can get a half-way view from looking from an angle, no?"
msgstr "Sedan kan vi få en vy halvvägs genom att titta från en vinkel, eller?"

#: ../../general_concepts/projection/orthographic_oblique.rst:45
msgid "If we do that for a lot of different sides…"
msgstr "Om vi gör det för många olika sidor ..."

#: ../../general_concepts/projection/orthographic_oblique.rst:50
msgid "And we line up the sides we get a…"
msgstr "Och om vi radar upp sidorna får vi en ..."

#: ../../general_concepts/projection/orthographic_oblique.rst:55
msgid ""
"But cubes are boring. I am suspecting that projection is so ignored because "
"no tutorial applies it to an object where you actually might NEED "
"projection. Like a face."
msgstr ""
"Men kuber är tråkiga. Jag misstänker att projektionen ignoreras helt "
"eftersom inga handledningar använder den med ett objekt där man faktiskt kan "
"BEHÖVA en projektion. Som ett ansikte."

#: ../../general_concepts/projection/orthographic_oblique.rst:57
msgid "First, let's prepare our front and side views:"
msgstr "Låt oss först förbereda vår framsida och sidovyerna:"

#: ../../general_concepts/projection/orthographic_oblique.rst:62
msgid ""
"I always start with the side, and then extrapolate the front view from it. "
"Because you are using Krita, set up two parallel rulers, one vertical and "
"the other horizontal. To snap them perfectly, drag one of the nodes after "
"you have made the ruler, and press the :kbd:`Shift` key to snap it "
"horizontal or vertical. In 3.0, you can also snap them to the image borders "
"if you have :menuselection:`Snap Image Bounds` active via the :kbd:`Shift + "
"S` shortcut."
msgstr ""
"Jag börjar alltid med sidan, och extrapolerar framsidan från den. Eftersom "
"Krita används, ställ in två parallella linjaler, en vertikal och en "
"horisontell. För att låsa dem perfekt, dra en av noderna efter linjalen har "
"skapats, och tryck på tangenten :kbd:`Skift`för att låsa den horisontellt "
"eller vertikalt. Med 3.0 kan man också låsa dem till bildkanterna om man "
"aktiverar :menuselection:`Lås bildgränser` via genvägen :kbd:`Skift` + :kbd:"
"`S`."

#: ../../general_concepts/projection/orthographic_oblique.rst:64
msgid ""
"Then, by moving the mirror to the left, you can design a front view from the "
"side view, while the parallel preview line helps you with aligning the eyes "
"(which in the above screenshot are too low)."
msgstr ""
"Därefter kan man konstruera en framsida från sidovyn genom att flytta "
"speglingen åt vänster, medan den parallella förhandsgranskningslinjen "
"hjälper till att justera ögonen (vilka är för låga i skärmbilden ovan)."

#: ../../general_concepts/projection/orthographic_oblique.rst:66
msgid "Eventually, you should have something like this:"
msgstr "Slutligen ska man få något som ser ut så här:"

#: ../../general_concepts/projection/orthographic_oblique.rst:71
msgid "And of course, let us not forget the top, it's pretty important:"
msgstr "Och låt oss inte glömma ovansidan, den är förstås rätt viktig:"

#: ../../general_concepts/projection/orthographic_oblique.rst:78
msgid ""
"When you are using Krita, you can just use transform masks to rotate the "
"side view for drawing the top view."
msgstr ""
"När man använder Krita kan man bara transformeringsmasker för att rotera "
"sidovyn för att rita ovansidan."

#: ../../general_concepts/projection/orthographic_oblique.rst:80
msgid ""
"The top view works as a method for debugging your orthos as well. If we take "
"the red line to figure out the orthographics from, we see that our eyes are "
"obviously too inset. Let's move them a bit more forward, to around the nose."
msgstr ""
"Ovansidan fungerar också som en metod för att felsöka ortografiken. Om vi "
"använder en röd linje för att räkna ut ortografiken från, ser vi att våra "
"ögon uppenbarligen är för djupa. Låt oss flytta dem lite mer framåt, till "
"omkring näsan."

#: ../../general_concepts/projection/orthographic_oblique.rst:85
msgid ""
"If you want to do precision position moving in the tool options docker, just "
"select 'position' and the input box for the X. Pressing down then moves the "
"transformed selection left. With Krita 3.0 you can just use the move tool "
"for this and the arrow keys. Using transform here can be more convenient if "
"you also have to squash and stretch an eye."
msgstr ""
"Om man vill göra precisionspositionering vid förflyttning i "
"verktygsalternativpanelen, välj bara 'position' och inmatningsrutan för X. "
"Att trycka ner flyttar därefter den transformerade markeringen åt vänster. "
"Med Krita 3.0 kan man bara använda flyttverktyget och piltangenterna för "
"det. Att använda transformering här kan vara bekvämare om man också måste "
"trycka ihop och sträcka ut ett öga."

#: ../../general_concepts/projection/orthographic_oblique.rst:90
msgid "We fix the top view now. Much better."
msgstr "Vi fixar ovansidan nu. Mycket bättre."

#: ../../general_concepts/projection/orthographic_oblique.rst:92
msgid ""
"For faces, the multiple slices are actually pretty important. So important "
"even, that I have decided we should have these slices on separate layers. "
"Thankfully, I chose to color them, so all we need to do is go to :"
"menuselection:`Layer --> Split Layer` ."
msgstr ""
"För ansikten är flera skivor faktiskt ganska viktiga. Till och med så "
"viktiga att jag har bestämt att vi ska ha skivorna på separata lager. Som "
"tur är valde jag att färglägga dem, så allt vi behöver göra är att gå till :"
"menuselection:`Lager --> Dela Lager`."

#: ../../general_concepts/projection/orthographic_oblique.rst:98
msgid ""
"This'll give you a few awkwardly named layers… rename them by selecting all "
"and mass changing the name in the properties editor:"
msgstr ""
"Det ger några klumpigt namngivna lager ... byt namn på dem genom att markera "
"allihopa och samtidigt ändra alla namnen i egenskapseditorn:"

#: ../../general_concepts/projection/orthographic_oblique.rst:103
msgid "So, after some cleanup, we should have the following:"
msgstr "Så efter lite städning, ska vi få följande:"

#: ../../general_concepts/projection/orthographic_oblique.rst:108
msgid "Okay, now we're gonna use animation for the next bit."
msgstr "Okej, nu ska vi använda animering för nästa bit."

#: ../../general_concepts/projection/orthographic_oblique.rst:110
msgid "Set it up as follows:"
msgstr "Ställ in den på följande sätt:"

#: ../../general_concepts/projection/orthographic_oblique.rst:115
msgid ""
"Both front view and side view are set up as 'visible in timeline' so we can "
"always see them."
msgstr ""
"Både framsidan och sidovyn är inställda som 'synliga på tidslinjen' så vi "
"kan alltid se dem."

#: ../../general_concepts/projection/orthographic_oblique.rst:116
msgid ""
"Front view has its visible frame on frame 0 and an empty frame on frame 23."
msgstr ""
"Framsidan har sin synliga bildruta på ruta 0 och en tom bildruta på ruta 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:117
msgid ""
"Side view has its visible frame on frame 23 and an empty view on frame 0."
msgstr ""
"Sidovyn har sin synliga bildruta på ruta 23 och en tom vy på bildruta 0."

#: ../../general_concepts/projection/orthographic_oblique.rst:118
msgid "The end of the animation is set to 23."
msgstr "Animeringens slut är inställd till 23."

#: ../../general_concepts/projection/orthographic_oblique.rst:123
msgid ""
"Krita can't animate a transformation on multiple layers on multiple frames "
"yet, so let's just only transform the top layer. Add a semi-transparent "
"layer where we draw the guidelines."
msgstr ""
"Krita kan inte ännu animera en transformering på flera lager och flera "
"bildrutor, så låt oss bara transformera det översta lagret. Lägg till ett "
"halvgenomskinligt lager där vi ritar hjälplinjerna."

#: ../../general_concepts/projection/orthographic_oblique.rst:125
msgid ""
"Now, select frame 11 (halfway), add new frames from front view, side view "
"and the guidelines. And turn on the onion skin by toggling the lamp symbols. "
"We copy the frame for the top view and use the transform tool to rotate it "
"45°."
msgstr ""
"Välj nu bilruta 11 (halvvägs), lägg till nya rutor från framsidan, sidovyn "
"och hjälplinjerna. Sätt på rispapper genom att växla lampsymbolerna. Vi "
"kopierar rutan för ovansidan och använder transformeringsverktyget för att "
"rotera den 45°."

#: ../../general_concepts/projection/orthographic_oblique.rst:130
msgid "So, we draw our vertical guides again and determine a in-between..."
msgstr ""
"Så vi ritar våra vertikala hjälplinjer igen och bestämmer en "
"mellanteckning ..."

#: ../../general_concepts/projection/orthographic_oblique.rst:135
msgid ""
"This is about how far you can get with only the main slice, so rotate the "
"rest as well."
msgstr ""
"Det är ungefär så långt man kan komma med bara huvudskivan, så rotera också "
"resten."

#: ../../general_concepts/projection/orthographic_oblique.rst:140
msgid "And just like with the cube, we do this for all slices…"
msgstr "Och precis som med kuben, gör vi det för alla skivor ..."

#: ../../general_concepts/projection/orthographic_oblique.rst:145
msgid ""
"Eventually, if you have the top slices rotate every frame with 15°, you "
"should be able to make a turn table, like this:"
msgstr ""
"Till sist, om man låter ovansidans skivor rotera varje ruta 15°, bör man "
"kunna skapa en skivtabell, så här:"

#: ../../general_concepts/projection/orthographic_oblique.rst:150
msgid ""
"Because our boy here is fully symmetrical, you can just animate one side and "
"flip the frames for the other half."
msgstr ""
"Eftersom vår pojke här är helt symmetrisk, kan man bara animera en sida och "
"vända rutorna för att få den andra halvan."

#: ../../general_concepts/projection/orthographic_oblique.rst:152
msgid ""
"While it is not necessary to follow all the steps in the theory section to "
"understand the tutorial, I do recommend making a turn table sometime. It "
"teaches you a lot about drawing 3/4th faces."
msgstr ""
"Även om det inte är nödvändigt att följa alla steg i teoridelen för att "
"förstå handledningen, rekommenderar jag att skapa en skivtabell ibland. Den "
"lär dig mycket om hur trefjärdedelsansikten ritas."

#: ../../general_concepts/projection/orthographic_oblique.rst:154
msgid "How about… we introduce the top view into the drawing itself?"
msgstr "Vad sägs om ... att vi introducerar översidan i själva teckningen?"
