# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-21 03:28+0200\n"
"PO-Revision-Date: 2019-06-21 10:24+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../<generated>:1
msgid "Subdivision"
msgstr "Underdelning"

#: ../../reference_manual/preferences/grid_settings.rst:1
msgid "Grid settings in Krita."
msgstr "Rutnätsinställningar i Krita."

#: ../../reference_manual/preferences/grid_settings.rst:15
msgid "Grid Settings"
msgstr "Rutnätsinställningar"

#: ../../reference_manual/preferences/grid_settings.rst:19
msgid "Deprecated in 3.0, use the :ref:`grids_and_guides_docker` instead."
msgstr "Avråds från i 3.0, använd :ref:`grids_and_guides_docker` istället."

#: ../../reference_manual/preferences/grid_settings.rst:21
msgid "Use :menuselection:`Settings --> Configure Krita --> Grid` menu item."
msgstr ""
"Använd menyalternativet :menuselection:`Inställningar -> Anpassa Krita -> "
"Rutnät`."

#: ../../reference_manual/preferences/grid_settings.rst:24
msgid ".. image:: images/preferences/Krita_Preferences_Grid.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Grid.png"

#: ../../reference_manual/preferences/grid_settings.rst:25
msgid "Fine tune the settings of the grid-tool grid here."
msgstr "Finjustera rutnätsverktygets inställningar här."

#: ../../reference_manual/preferences/grid_settings.rst:28
msgid "Placement"
msgstr "Placering"

#: ../../reference_manual/preferences/grid_settings.rst:30
msgid "The user can set various settings of the grid over here."
msgstr "Användaren kan ställa in diverse inställningar av rutnätet här."

#: ../../reference_manual/preferences/grid_settings.rst:32
msgid "Horizontal Spacing"
msgstr "Horisontellt mellanrum"

#: ../../reference_manual/preferences/grid_settings.rst:33
msgid ""
"The number in Krita units, the grid will be spaced in the horizontal "
"direction."
msgstr ""
"Värdet i Kritas enheter som rutnätet kommer att delas upp i horisontell "
"riktning."

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid "Vertical Spacing"
msgstr "Vertikalt mellanrum"

#: ../../reference_manual/preferences/grid_settings.rst:35
msgid ""
"The number in Krita units, the grid will be spaced in the vertical "
"direction. The images below will show the usage of these settings."
msgstr ""
"Värdet i Kritas enheter som rutnätet kommer att delas upp i vertikal "
"riktning. Bilderna nedan visar användning av inställningarna."

#: ../../reference_manual/preferences/grid_settings.rst:37
msgid "X Offset"
msgstr "X-position"

#: ../../reference_manual/preferences/grid_settings.rst:38
msgid "The number to offset the grid in the X direction."
msgstr "Värdet som rutnätet ska förflyttas i X-riktningen."

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "Y Offset"
msgstr "Y-position"

#: ../../reference_manual/preferences/grid_settings.rst:40
msgid "The number to offset the grid in the Y direction."
msgstr "Värdet som rutnätet ska förflyttas i Y-riktningen."

#: ../../reference_manual/preferences/grid_settings.rst:42
msgid ""
"Some examples are shown below, look at the edge of the image to see the "
"offset."
msgstr ""
"Några exempel visas nedan. Titta på bildens kant för att se förskjutningen."

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid "Subdivisions"
msgstr "Underdelningar"

#: ../../reference_manual/preferences/grid_settings.rst:45
msgid ""
"Here the user can set the number of times the grid is subdivided. Some "
"examples are shown below."
msgstr ""
"Här kan användaren ställa in hur många gånger som rutnätet delas upp. Några "
"exempel visas nedan."

#: ../../reference_manual/preferences/grid_settings.rst:48
msgid "Style"
msgstr "Stil"

#: ../../reference_manual/preferences/grid_settings.rst:50
msgid "Main"
msgstr "Huvudstil"

#: ../../reference_manual/preferences/grid_settings.rst:51
msgid ""
"The user can set how the main lines of the grid are shown. Options available "
"are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"Användaren kan ställa in hur rutnätets huvudlinjer visas. Tillgängliga "
"alternativ är Linjer, Streckade linjer, Punkter. Färgen kan också ställas in "
"här."

#: ../../reference_manual/preferences/grid_settings.rst:53
msgid ""
"The user can set how the subdivision lines of the grid are shown. Options "
"available are Lines, Dashed Lines, Dots. The color also can be set here."
msgstr ""
"Användaren kan ställa in hur rutnätets underdelningslinjer visas. "
"Tillgängliga alternativ är Linjer, Streckade linjer, Punkter. Färgen kan "
"också ställas in här."
